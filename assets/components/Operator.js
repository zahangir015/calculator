import React, {Component} from 'react';

class Operator extends Component {
    render() {
        return (
            <>
                <button type="button" className="operator btn btn-info"
                        onClick={() => this.props.handleOperatorClick('+')}>&#128125;</button>
                <button type="button" className="operator btn btn-info"
                        onClick={() => this.props.handleOperatorClick('-')}>&#128128;</button>
                <button type="button" className="operator btn btn-info"
                        onClick={() => this.props.handleOperatorClick('*')}>&#128123;</button>
                <button type="button" className="operator btn btn-info"
                        onClick={() => this.props.handleOperatorClick('/')}>&#128561;</button>
            </>
        )
    }
}

export default Operator;