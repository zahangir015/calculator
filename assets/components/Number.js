import React, {Component} from 'react';

class Number extends Component {
    render() {
        return (
            <>
                <button type="button" value="7" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(7)}>7
                </button>
                <button type="button" value="8" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(8)}>8
                </button>
                <button type="button" value="9" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(9)}>9
                </button>
                <button type="button" value="4" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(4)}>4
                </button>
                <button type="button" value="5" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(5)}>5
                </button>
                <button type="button" value="6" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(6)}>6
                </button>
                <button type="button" value="1" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(1)}>1
                </button>
                <button type="button" value="2" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(2)}>2
                </button>
                <button type="button" value="3" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(3)}>3
                </button>
                <button type="button" value="0" className="btn btn-light waves-effect"
                        onClick={() => this.props.handleNumberClick(0)}>0
                </button>
            </>
        )
    }
}

export default Number;