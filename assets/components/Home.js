import React, {Component} from 'react';
import axios from "axios";
import Operator from "./Operator";
import Number from "./Number";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sum: 0,
            equation: '',
            display: ''
        };
    }

    operatorIcons = {'+': '&#128125;', '-': '&#128128;', '*': '&#128123;', '/': '&#128561;'}

    handleOperatorClick = (operator) => {
        let {equation: equationString, display: displayString} = this.state;
        if ((equationString.length === 0) && (operator === '-')) {
            equationString += operator
            displayString += ' ' + this.operatorIcons[operator] + ' '
        } else if (equationString.length > 0) {
            let lastCharacter = equationString.charAt(equationString.length - 1)
            if (!(lastCharacter in this.operatorIcons)) {
                equationString += operator
                displayString += ' ' + this.operatorIcons[operator] + ' '
            }
        }
        this.setState({equation: equationString, display: displayString})
    }

    handleNumberClick = (number) => {
        let {equation: equationString, display: displayString} = this.state;
        equationString += number
        displayString += number
        this.setState({equation: equationString, display: displayString})
    }

    handleAllClean = () => {
        let {equation: equationString, display: displayString} = this.state;
        equationString = ''
        displayString = ''
        this.setState({equation: equationString, display: displayString, sum: 0})
    }

    getSum = () => {
        axios.get(`http://127.0.0.1:8000/api/sum`, {
            params: {
                equation: this.state.equation,
            }
        }).then(sum => {
            if (sum.status === 200) {
                this.setState({sum: sum.data, display: sum.data})
            } else {
                alert(sum.statusText)
                this.handleAllClean()
            }
        })
    }

    render() {
        return (
            <div className="container my-4">
                <div className="calculator">
                    <div className="calculator-screen z-depth-1" dangerouslySetInnerHTML={{
                        __html: this.state.display
                    }
                    }></div>


                    <div className="calculator-keys">
                        <Operator handleOperatorClick={this.handleOperatorClick}/>
                        <Number handleNumberClick={this.handleNumberClick}/>

                        {/*<button type="button" className="decimal function btn btn-secondary" >CE</button>*/}
                        <button type="button" className="all-clear function btn btn-danger btn-sm"
                                onClick={() => this.handleAllClean()}>AC
                        </button>
                        <button type="button" className="equal-sign operator btn btn-default"
                                onClick={() => this.getSum()}>=
                        </button>

                    </div>
                </div>
            </div>
        )
    }
}

export default Home;