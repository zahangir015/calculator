<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @Route("/{reactRouting}", name="home", defaults={"reactRouting": null})
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/api/sum", name="sum")
     * @return JsonResponse
     */
    public function getSum(Request $request): JsonResponse
    {
        $equation = $request->get('equation');
        $math_string = "return ".$equation.";";
        $sum = eval($math_string);
        return new JsonResponse($sum);
    }
}
