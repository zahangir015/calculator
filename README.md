# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Calculator with Emoji
* Version 1.0

### How do I get set up? ###

* Clone from -> git clone https://zahangir015@bitbucket.org/zahangir015/calculator.git
* Run this command in the project directory ->  composer install/update
* Install Symfony CLI
* Install Symfony Webpack Encore -> composer require symfony/webpack-encore-bundle
* Run this command in the project directory -> yarn install
* Start server -> symfony server:start / symfony server:run
* Run the following command to compile the React application and watch the JavaScript files for any changes ->
  yarn encore dev --watch

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact